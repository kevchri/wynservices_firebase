import 'package:flutter/material.dart';
import 'package:wynservices/src/pages/InicioSesion_page.dart';
import 'package:wynservices/src/pages/admin_page.dart';
import 'package:wynservices/src/pages/registro_page.dart';
void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WYN Services',
      initialRoute: 'admin',
      routes: {
        'admin': (BuildContext context)=>AdminPage(),
        'registro': (BuildContext context)=>RegistroPage(),
        'iniciosesion':(BuildContext context)=>InicioSecionPage(),
      },
    );
  }
}